#!/usr/bin/env bash

session='ansible'

tmux new-session -d -s $session

for server in $(ls -1 ./playbooks); do
    tmux new-window -t $session -n "$server" "ansible-playbook --check './playbooks/$server';read"
done

tmux attach -t $session
