#!/usr/bin/env bash

yt() {
	notify-send "URL - Youtube" "$1" -t 2500
	# nohup termite -e "mpv '$1'" > /dev/null 2>&1 || notify-send -u critical "Youtube - Erreur" "$1" &
	# mpv "$1" > /dev/null 2>&1 || notify-send -u critical "Youtube - Erreur" "$1" &
	mpv "$1" || notify-send -u critical "Youtube - Erreur" "$1"
}

mpv_term() {
	notify-send "URL - Vidéo" "$1" -t 2500
	# nohup termite -e "mpv '$1'" > /dev/null 2>&1 || notify-send -u critical "MPV - Erreur" "$1" &
	# mpv "$1" > /dev/null 2>&1 || notify-send -u critical "MPV - Erreur" "$1" &
	mpv "$1" || notify-send -u critical "MPV - Erreur" "$1"
}

browser() {
	notify-send "URL - Firefox" "$1" -t 2500
	# nohup firefox --private-window "$1" > /dev/null 2>&1 &
	firefox --new-window "$1"
}

image() {
	notify-send "URL - Image" "$1" -t 2500
	# nohup curl "$1" | imv - > /dev/null || notify-send -u critical "Image - Erreur" "$1" &
	curl "$1" | imv - || notify-send -u critical "Image - Erreur" "$1"
}

twitter() {
	# notify-send "URL - twitter" "$1" -t 2500
	# nohup termite -e "python /home/hs-157/Documents/dev/twitter-terminal/tui.py '$1'" > /dev/null 2>&1 || notify-send -u critical "Twitter - Erreur" "$1" &
	browser "$@"
}


URI=${@,,}

echo "$(date) : $@ - $URI" >> /tmp/open_link.sh

case $URI in
	*.jpg|*.jpeg|*.png|*.bmp|*.svg|*.jfif)
		image "$@" || browser "$@" ;;
	*.webm|*.mp4|*.ogv|*.gif|*.gifv|*.mp3|*.webp|*.mkv|*.mov|*.m4v|*.ogg|*.wav|*.flac)
		mpv_term "$@" || browser "$@" ;;
	*youtube.com/*|*youtu.be/*)
		yt "$@" ;;
	*.html)
		browser "$@" ;;
	*txt|*//sprunge.us/*|*//ix.io/*|*//ptpb.pw/*)
		nohup termite -e sh -c "curl -s '$@' | less -r" > /dev/null || firefox --new-window "$@" > /dev/null 2>&1 &;;
	*//*twitter.com/*/status/*)
		twitter "$@" ;;
	*)
		browser "$@" ;;
esac

exit 1
