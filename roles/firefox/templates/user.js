{{ ansible_managed | comment("c") }}

// Configuration Firefox : https://arkenfox.github.io/gui/

// Déactiver le warning sur la page about:config
user_pref("browser.aboutConfig.showWarning", false);

// Ouvrir les fenêtres et onglets précédents
user_pref("browser.startup.page", 3);

// Avertir lors de la fermeture de plusieurs onglets
user_pref("browser.tabs.warnOnClose", true);

// Utiliser les paramètres de votre système d’exploitation pour formater les dates, les heures, les nombres et les mesures
user_pref("intl.regional_prefs.use_os_locales", true);

// Demander s’il faut ouvrir ou enregistrer les fichiers
user_pref("browser.download.always_ask_before_handling_new_types", true);

// Lire le contenu protégé par des DRM
user_pref("media.eme.enabled", true);

// Utiliser le défilement doux
user_pref("general.smoothScroll", false);

// Page d’accueil et nouvelles fenêtres
user_pref("browser.startup.homepage", "about:blank");

// Nouveaux onglets (about:blank)
user_pref("browser.newtabpage.enabled", false);

// Contenu de la Page d’accueil de Firefox (désactivation)
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.showSearch", false);

// Moteur de recherche par défaut
user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("browser.urlbar.placeholderName.private", "DuckDuckGo");

// Barre d’adresse - Les moteurs de recherche
user_pref("browser.urlbar.suggest.engines", false);

// Demander aux sites web de ne pas vendre ni partager mes données
user_pref("privacy.globalprivacycontrol.enabled", true);
user_pref("privacy.globalprivacycontrol.was_ever_enabled", true);

// Demander aux sites web de « Ne pas me pister »
user_pref("privacy.donottrackheader.enabled", true);

// Proposer d’enregistrer les mots de passe
user_pref("signon.rememberSignons", false);

// Préférences publicitaires des sites web
user_pref("dom.private-attribution.submission.enabled", false);

// Remplissage automatique
user_pref("extensions.formautofill.addresses.enabled", false);
user_pref("extensions.formautofill.creditCards.enabled", false);
user_pref("signon.autofillForms", false);

// Traduction
user_pref("browser.translations.enable", false);
user_pref("browser.translations.automaticallyPopup", false);

// Web Speech
user_pref("media.webspeech.synth.enabled", false);
