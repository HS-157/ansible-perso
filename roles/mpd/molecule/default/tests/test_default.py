"""Role testing files using testinfra."""

def test_hosts_file(host):
    """Validate /etc/hosts file."""
    f = host.file("/etc/hosts")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"

def test_installed_package(host):
    packages = [
            "mpd"
            ]
    for p in packages:
        package = host.package(p)
        assert package.is_installed == True

def test_mpd_conf(host):
    mpd_conf = host.file("/home/vagrant/.config/mpd/mpd.conf")
    assert mpd_conf.exists == True

# def test_service_mpd(host):
#     s = host.service("mpd")
#     assert s.is_valid == True
#     assert s.is_enabled == True
#     assert s.is_running == True


def test_mpd_conf(host):
    s = host.socket("tcp://6600")
    assert s.is_listening == True
