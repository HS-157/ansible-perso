{{ ansible_managed | comment }}

{% if zsh_sway %}
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  export MOZ_ENABLE_WAYLAND=1
  export XDG_CURRENT_DESKTOP=sway
  exec sway 2>> /tmp/sway.log
fi
{% endif %}

export PATH=$HOME/.local/bin:$PATH
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

export SSH_ASKPASS_REQUIRE="prefer"
export SSH_ASKPASS="/usr/bin/pass-askpass"

autoload -Uz compinit promptinit
compinit
promptinit

# This will set the default prompt to the walters theme
prompt walters

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

eval "$(starship init zsh)"

[[ -s "/etc/grc.zsh" ]] && source /etc/grc.zsh

if [ -f ~/.aliases ]; then
    source ~/.aliases
fi

bindkey '^[OA' history-beginning-search-backward
bindkey '^[OB' history-beginning-search-forward
zstyle ':completion:*:complete:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'

# pass-askpass - https://forge.dotslashplay.it/HS-157/pass-askpass
export PASS_ASKPASS_PATH="ssh/$(hostnamectl hostname)"

# virsh -c qemu:///system
export VIRSH_DEFAULT_CONNECT_URI="qemu:///system"
