"""Role testing files using testinfra."""

def test_hosts_file(host):
    """Validate /etc/hosts file."""
    f = host.file("/etc/hosts")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"

def test_installed_package(host):
    packages = [
            "zsh",
            "zsh-completions",
            "fzf",
            "starship"
            ]
    for p in packages:
        package = host.package(p)
        assert package.is_installed == True

def test_zshrc(host):
    zshrc = host.file("/home/vagrant/.zshrc")
    assert zshrc.exists == True
