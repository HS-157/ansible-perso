#!/usr/bin/env python3

from pathlib import Path

def main():
    not_managed = []
    roles = Path("./roles")
    # templates = roles.glob("roles/**/template/*")
    a = roles.glob('**/*')
    templates = [t for t in a if t.parent.name.endswith("templates")]
    for t in templates:
        with t.open() as f:
            managed = False
            for _ in range(3):
                line = f.readline()
                if line.startswith("{{ ansible_managed | comment"):
                    managed = True
            if not managed:
                not_managed.append(t)

    if not_managed:
        print("> File without comment « managed by Ansible »")
        print("\n".join(t.as_posix() for t in not_managed))
        exit(1)

if __name__ == "__main__":
    main()
