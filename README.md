# ansible-perso

Personal Ansible role for my machines / servers.

[![pipeline status](https://framagit.org/HS-157/ansible-perso/badges/master/pipeline.svg)](https://framagit.org/HS-157/ansible-perso/-/commits/master)

## Roles

* [grim](roles/grim) : Screenshot capture for [Sway](https://swaywm.org/) and work with [Slurp](https://github.com/emersion/slurp).
* [mako](roles/mako) : Lightweight notification daemon for [Sway](https://swaywm.org/).
* [mpd](roles/mpd) : Music player server.
* [mpv](roles/mpv) : Media player for the command line.
* [ncmpcpp](roles/ncmpcpp) : NCurses Music Player Client (Plus Plus).
* [nftables](roles/nftables) : Linux firewall.
* [openvpn](roles/openvpn) : OpenVPN client.
* [pikaur](roles/pikaur) : AUR helper write in Python.
* [ranger](roles/ranger) : A VIM-inspired filemanager for the console .
* [ssh](roles/ssh) : OpenSSH server.
* [sway](roles/sway) : Tiling Wayland compositor like [i3wm](https://i3wm.org/)
* [tmux](roles/tmux) : Terminal multiplexer.
* [vim](roles/vim) : Neovim : Vim-fork.
* [waybar](roles/waybar) : Highly customizable Wayland bar for [Sway](https://swaywm.org/).
* [wget](roles/wget) : HTTP / HTTPS command-line client.
* [wireguard-server](roles/wireguard-server) : Wireguard VPN server.
* [wireguard](roles/wireguard) : Wireguard VPN client.
* [yt-dl](roles/yt-dl) : Command-line program to download videos.
* [zsh](roles/zsh) : Z shell.

## One shot

This scripts used for provisioning one time.

### [incus/base.yml](one_shot/incus/base.yml)

This script use for provisioning Inucs containers with minimal configuration.

You need execute this script on Incus host.
~~~
sudo incus list -c n -f csv > /tmp/incus
sudo ansible-playbook -i /tmp/incus ./one_shot/incus/base.yml
~~~

## Notes

### Wireguard

Generate a pair of keys for new server :

~~~
wg genkey | pass insert -e servers/<server>/wireguard/key
pass servers/<server>/wireguard/key | wg pubkey
~~~

## SSH

Generate a SSH key for new server :

~~~
ssh-keygen -t ed25519 -b 521 -C "$(whoami)@$(hostnamectl hostname)-$(date -I)" -f ~/.ssh/keys/<server>
~~~

## Requirements

* [Ansible](https://www.ansible.com)
* [Ansible-aur](https://github.com/kewlfft/ansible-aur) for [pikaur](roles/pikaur) role

## Requirements test

* [Molecule](https://molecule.readthedocs.io)
* [Molecule-vagrant](https://github.com/ansible-community/molecule-vagrant)
* [Vagrant](https://www.vagrantup.com/)
* [Vagrant-libvirt](https://github.com/vagrant-libvirt/vagrant-libvirt)
* [Infratest](https://testinfra.readthedocs.io)

## License

* *BSD 2-clause "Simplified" License* ([LICENSE](LICENSE))

## Role template

I use [copier](https://github.com/copier-org/copier) for create role from my [personnal template](https://lab.shelter.moe/HS-157/copier-ansible).
